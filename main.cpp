#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "lista_estatica.h"


List cpyList(List l){
  return l;
}

bool decrescent(int a, int b){
  return a < b;
}

bool crescent(int a, int b){
  return a > b;
}

int main(int argc, char **argv){
  List acoes = createList();
  
  insert(acoes, rand() % 3 + 5);
  
  srand (time(NULL));
  int variacao = 0;
  for(int i = 9; i < 15; i++){
    for(int j = 0; j < 60; j++){
      if(j % 20 == 0){
	 variacao = rand() % 6 - 3;
	 variacao += getElement(acoes, length(acoes)-1);
	 insert(acoes, variacao);
	 std::cout << showList(acoes) << std::endl;
      }
    }
  }
  
  std::cout << showList(acoes) << std::endl;
  sortList(acoes, decrescent);
  std::cout << showList(acoes) << std::endl;
  
  
  
}



// void jogo(List& jogador1, List& jogador2, int rodadas);
// int leUmNumeroInt(int range);


// int main(int argc, char **argv) {  
//   List jogador1 = createList();
//   List jogador2 = createList();
//   
//   int rodadas = leUmNumeroInt(maxSize(jogador1));
//   jogo(jogador1,jogador2,rodadas);
//   
//   std::cout << "Jogador 1: "
//   << showList(jogador1) << std::endl
//   << "Jogador 2: "
//   << showList(jogador2) << std::endl;
//   return 0;
// }


int leUmNumeroInt(int range){
  int num = 0;
  std::ostringstream s;
  s << range;
  do{
    std::cout << "Digite um numero inteiro maior que 0  e menor que "+s.str()+": ";
    std::cin >> num;
  }while(num >= range || num <= 0);
  
  return num;
}


void jogo(List& jogador1, List& jogador2, int rodadas){
  const int PAPEL = 1;
  const int PEDRA = 3;
  std::string jogadas[4] = {"Jogada: ","Papel", "Tesoura", "Pedra"};
  
  int jogadaJogador1;
  int jogadaJogador2;
  std::string resultado;
    srand (time(NULL));
  for(int i=0; i < rodadas ; i++){
    int ganhador1 = 0;
    int ganhador2 = 0;
    jogadaJogador1 = rand() % 3 + 1;
    jogadaJogador2 = rand() % 3 + 1;
    
    if(jogadaJogador1 == jogadaJogador2){
      insert(jogador1, jogadaJogador1);
      insert(jogador2, jogadaJogador2);
      ganhador1++;
      resultado =  jogadas[0] + "1 -" + jogadas[jogadaJogador1] + ", 2- " + jogadas[jogadaJogador2]+ ": Empate, Jogador 1 ganha";
    }
    else{
      if(jogadaJogador1 == PAPEL && jogadaJogador2 == PEDRA){
	ganhador1++;
	resultado = jogadas[0] + "1 -" + jogadas[jogadaJogador1] + ", 2- " + jogadas[jogadaJogador2]+ ": Jogador 1 ganha";
	insert(jogador1, jogadaJogador1);
	insert(jogador2, jogadaJogador2);
      }
      else{
	if(jogadaJogador1 == PEDRA && jogadaJogador2 == PAPEL){
	  ganhador2++;
	  resultado = jogadas[0] + "1 -" + jogadas[jogadaJogador1] + ", 2- " + jogadas[jogadaJogador2]+ ": Jogador 2 ganha";
	  insert(jogador1, jogadaJogador1);
	  insert(jogador2, jogadaJogador2);
	}
	else{
	  if(jogadaJogador1 > jogadaJogador2){
	    ganhador1++;
	    resultado = jogadas[0] + "1 -" + jogadas[jogadaJogador1] + ", 2- " + jogadas[jogadaJogador2]+ ": Jogador 1 ganha";
	    insert(jogador1, jogadaJogador1);
	    insert(jogador2, jogadaJogador2);
	  }
	  else{
	    ganhador2++;
	    resultado = jogadas[0] + "1 -" + jogadas[jogadaJogador1] + ", 2- " + jogadas[jogadaJogador2]+ ": Jogador 2 ganha";
	    insert(jogador1, jogadaJogador1);
	    insert(jogador2, jogadaJogador2);
	  }
	}
      }
    }
    std::cout << resultado << std::endl;
    
    if(ganhador1 > ganhador2)
      std::cout << "Jogador 1 ganhou" << std::endl;
    else
      std::cout << "Jogador 2 ganhou" << std::endl;
    
  }
}
