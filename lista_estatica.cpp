#include "lista_estatica.h"


List createList(){
  List list;
  list.length = 0;
 return list; 
};


List copyList(List list){
  return list;
}


void sortList(List& list, bool (*callback)(int, int)){
  int aux = 0;
  for(int i = 0; i < length(list) ; i++ ){
    for(int j =0; j < length(list) - 1 ; j++ ){
      if(callback(getElement(list, j), getElement(list,j+1))){
	aux = getElement(list, j);
	insertPos(list, getElement(list, j+1), j);
	insertPos(list, aux, j+1);
      }
      
    }
  }
}


void insertPos(List& list, int elem, int pos){
  list.list[pos] = elem;
  
}

int maxSize(List& list){
  return MAX;
};

void destroiList(List& list){
  list.length = 0;
};
bool isEmpty(const List& list){
  return list.length == 0;
};
bool hasSpace(List& list){
  return list.length < MAX;
};
int length(List& list){
  return list.length;
};
bool has(List& list,int elem){
 for(int i=0; i < length(list); i++ ){
   if(list.list[i] == elem)
     return true;
 }
 return false;
};
bool isValid(List& list, int pos){
  
  return pos > 0 && pos < list.length;
};
int getElement(List& list, int pos){
  return list.list[pos];
  
};
int getPosition(List& list, int elem){
  for(int i=0; i < length(list); i++ ){
   if(list.list[i] == elem)
     return i;
 }
 return -1;
  
};
void insert(List& list, int elem){
  if(hasSpace(list)){
    list.list[length(list)] = elem;
    list.length++;
  }
};
void remove(List& list, int elem){
  int len = length(list) -1;
  int pos = getPosition(list, elem);

  if(pos == len){
    list.length--;
    return;
  }
  if(pos > -1){
    for(int j = pos; j < len; j++ ){
        list.list[j] = list.list[j+1];
       }
    list.length--;
  }
};
std::string showList(List& list){
  std::ostringstream s;
  for(int i=0; i < length(list); i++ ){
    s << list.list[i] << " ";
 }
  return s.str();
};