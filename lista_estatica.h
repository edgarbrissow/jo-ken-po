#ifndef LISTA_ESTATICA_H
#define LISTA_ESTATICA_H
#include <string>
#include <sstream>
const unsigned MAX = 1000;
struct List{
  int list[MAX];
  int length;
};

List createList();
List copyList(List list);
void sortList(List& list, bool (*callback)(int, int));
void insertPos(List& list, int elem, int pos);
void destroiList(List& list);
int maxSize(List& list);
bool isEmpty(List& list);
bool hasSpace(List& list);
int length(List& list);
bool has(List& list,int elem);
bool isValid(List& list, int pos);
int getElement(List& list, int pos);
int getPosition(List& list, int elem);
void insert(List& list, int elem);
void remove(List& list, int elem);
std::string showList(List& list);








#endif